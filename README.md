# MorphTemplates

## Overview

MorphTemplates is a library that provides a declarative way of developing Morph widgets, based on concepts found in functional reactive UI toolkits, like React.js.

## Install

```Smalltalk
Feature require: 'MorphTemplates'
```

Also, file in:

* `FlowLayoutMorph.st`
* `PluggableButtonMorphPlus.st`

## Morph templates

The idea is to describe the Morphic view using templates. Morph templates are declarative descriptions of Morphs composition. It is declarative because templates describes how the Morph should be composed, without the need to specify the updates to the view, the updates to the view are managed automatically; very similar to how React.js works.

Subclasses of `MorphTemplateMorph` are rendered using templates. The state of the morph is initialized in #initialize method and instead of implementing `Morph>>drawOn:` method, `renderOn:` method should be implemented instead. `renderOn:` method receives a `MorphTemplate` and is supposed to use its API to build a description of the Morph view.

For example:

```Smalltalk
MorphTemplateMorphExample>>renderOn: aTemplate
	aTemplate morphClass: FlowLayoutMorph;
		      createWith: #newColumn;
			  child: [:btn | btn morphClass: PluggableButtonMorph;
								initialize: [:b |  b layoutSpec: LayoutSpec useAll;
								                       model: self;
								                       action: #buttonPushed;
												morphExtent: 100@20];
							     prop: #label: value: ((status = #welcome) 
								 	ifTrue: ['Hello']
									ifFalse: ['World'])]
```
In the above example, a `FlowLayoutMorph` is going to be the container morph, created via `#newColumn` message, with a `PluggableButtonMorph` as child.
The label of the button depends on the `status` instance variable and displays either `Hello` or `World` depending on its value.
When the morph state changes, the view is updated automatically. In this case, when `status` changes, the label of the mor changes to `Hello` or `World`. 
To indicate a status change, #changed message should be invoked:

```Smalltalk
MorphTemplateMorphExample>>buttonPushed
	(status = #welcome) 
		ifTrue: [status := #bye]
		ifFalse: [ status := #welcome].
	self changed.
```

The interesting thing is that there was no need of manually updating the label when the state changes, that is taken care of by the library. This is how React.js works.

## Examples

There are several examples available. The easiest way to try them is from the `World Menu`->`"New morph..."`->`MorphTemplates`
