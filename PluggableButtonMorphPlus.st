'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 25 December 2018 at 6:15:54.240409 pm'!
!classDefinition: #PluggableButtonMorphPlus category: #'Mold-UI'!
PluggableButtonMorph subclass: #PluggableButtonMorphPlus
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Mold-UI'!

!PluggableButtonMorphPlus methodsFor: 'as yet unclassified' stamp: 'MM 6/13/2017 10:40'!
performAction
	"Inform the model that this button has been pressed. "

	actionSelector ifNotNil: [
		actionSelector isSymbol ifTrue: [
			model perform: actionSelector ]
		ifFalse: [
			actionSelector value	]]! !
