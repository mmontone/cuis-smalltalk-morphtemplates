'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 15 January 2018 at 11:49:00.853138 am'!
!classDefinition: #FlowLayoutMorph category: #'Morphic-Layouts'!
LayoutMorph subclass: #FlowLayoutMorph
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Morphic-Layouts'!

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'Mariano Montone 1/15/2018 11:47'!
calculateColumnExtent
	| width height |
	
	height := 0.
	width := 0.
	
	self visibleSubmorphsDo: [ :sm | | smMinExtent |
				smMinExtent := sm morphExtent.
				"use maximum width across submorphs"
				width := width max: smMinExtent x. 
				"sum up submorph heights"
				height := height + smMinExtent y + self ySeparation. 
			].
		
	height := height - self ySeparation.
		   
	^ (width @ height) "+ self extentBorder".! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'Mariano Montone 1/15/2018 11:47'!
calculateMinimumColumnExtent
	| width height |
	
	height := 0.
	width := 0.
	
	self visibleSubmorphsDo: [ :sm | | smMinExtent |
				smMinExtent := sm minimumExtent.
				"use maximum width across submorphs"
				width := width max: smMinExtent x. 
				"sum up submorph heights"
				height := height + smMinExtent y + self ySeparation. 
			].
		
	height := height - self ySeparation.
		   
	^ (width @ height) "- self extentBorder".! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'Mariano Montone 1/15/2018 11:47'!
calculateMinimumRowExtent
	| width height |
	
	height := 0.
	width := 0.
	
	self visibleSubmorphsDo: [ :sm | | smMinExtent |
				smMinExtent := sm minimumExtent.
				"use maximum width across submorphs"
				height := height max: smMinExtent y. 
				"sum up submorph heights"
				width := width + smMinExtent x + self xSeparation. 
			].
	width := width - self xSeparation.
		
	^ (width @ height)  "+ self extentBorder".! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'Mariano Montone 1/15/2018 11:47'!
calculateRowExtent
	| width height |
	
	height := 0.
	width := 0.
	
	self visibleSubmorphsDo: [ :sm | | smMinExtent |
				smMinExtent := sm morphExtent.
				"use maximum width across submorphs"
				height := height max: smMinExtent y. 
				"sum up submorph heights"
				width := width + smMinExtent x + self xSeparation. 
			].
		
	width := width - self xSeparation.
		
	^ (width @ height)  "+ self extentBorder".! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 1/2/2018 14:09'!
initialize
	super initialize! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MM 5/2/2016 21:18'!
layoutBounds
	"Return the bounds for laying out children of the receiver"

	^0@0 extent: extent! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 1/2/2018 14:32'!
minimumExtent
	"Answer my cached minimumExtent.
	This may be expensive to calculate, so I only do that as required."
	
	"cachedMinExtent 
		ifNil: [ cachedMinExtent := self direction == #horizontal 
											ifTrue: [self calculateMinimumRowExtent ]
											ifFalse: [self calculateMinimumColumnExtent ]].
			
	^ cachedMinExtent"
	^self morphExtent ! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'MarianoMontone 1/2/2018 14:33'!
morphExtent
	^self direction == #horizontal 
		ifTrue: [self calculateRowExtent ]
		ifFalse: [self calculateColumnExtent ]! !

!FlowLayoutMorph methodsFor: 'as yet unclassified' stamp: 'Mariano Montone 1/15/2018 11:47'!
visibleSubmorphsDo: aBlock

	^ self submorphs do: [:m |
		m visible ifTrue: [aBlock value: m]]
		! !



!FlowLayoutMorph methodsFor: 'layout' stamp: 'MarianoMontone 1/2/2018 15:42'!
layoutSubmorphsHorizontallyIn: bounds

	| width submorphsToLayout nextMorph currentPosition nextX currentRow rowHeight |
	
	width := self morphExtent x.
	
	currentPosition := 0@0.
	
	submorphsToLayout := self submorphsToLayout.
	
	currentRow := OrderedCollection new.
	
	1 to: submorphsToLayout size do: [ :index |
		
		nextMorph := submorphsToLayout at: index.
		
		nextMorph morphPosition: currentPosition.
		
		currentRow add: nextMorph.
		
		nextX := (currentPosition x) + self xSeparation + nextMorph morphExtent x.
		
		nextX > width ifTrue: [
			rowHeight := currentRow inject: 0 into: [:h :m | m morphExtent y max: h].
			
			currentRow do: [:m | |ls ht|
				ls := m layoutSpec.
				ht := (ls heightFor: rowHeight).
				m morphExtent: m morphExtent x @ ht
			].
		
			currentPosition := 0 @ (currentPosition y + rowHeight).
			currentRow := OrderedCollection new.
			] 
		ifFalse: [
			currentPosition := nextX @ currentPosition y]].
	
	rowHeight := currentRow inject: 0 into: [:h :m | m morphExtent y max: h].
	
	"currentRow do: [:m | |ls ht|
				ls := m layoutSpec.
				ht := (ls heightFor: rowHeight).
				m morphExtent: m morphExtent x @ ht
			]"! !

!FlowLayoutMorph methodsFor: 'layout' stamp: 'MarianoMontone 1/2/2018 15:42'!
layoutSubmorphsVerticallyIn: bounds

	| height submorphsToLayout nextMorph currentPosition nextY currentCol colWidth |
	
	height := self morphExtent y.
	
	currentPosition := 0@0.
	
	submorphsToLayout := self submorphsToLayout.
	
	"submorphsToLayout do: [:m | m fitContents]."
	
	currentCol := OrderedCollection new.
	
	1 to: submorphsToLayout size do: [ :index |
		
		nextMorph := submorphsToLayout at: index.
		
		nextMorph morphExtent: self morphExtent x @ nextMorph morphExtent y.
		
		nextMorph morphPosition: currentPosition.
		
		currentCol add: nextMorph.
		
		nextY := (currentPosition y) + self ySeparation + nextMorph morphExtent y.
		
		"nextY > height ifTrue: [
			colWidth := currentCol inject: 0 into: [:w :m | m morphExtent x max: w].
			currentPosition := (currentPosition x + colWidth) @ 0.
			currentCol := OrderedCollection new.] 
		ifFalse: ["
			currentPosition := currentPosition x @ nextY"]"
		].
	
	colWidth := currentCol inject: 0 into: [:w :m | m morphExtent x max: w]! !
